import React, { FunctionComponent } from "react";

import Bonus from "./Bonus";
import Number from "./Number";

import { rowClickHandler, IRowData } from "../types";

interface IRowElementProps {
  index: number;
  handler: rowClickHandler;
  lockHandler: Function;
  isGameOver: boolean;
}

type RowProps = IRowData & IRowElementProps;

const Row: FunctionComponent<RowProps> = ({
  boxes,
  locked,
  color,
  handler,
  lockHandler,
  isGameOver,
}) => {
  return (
    <div className={`Row ${color}`}>
      <div className="NumberWrapper">
        {boxes.map((box, index) => {
          return (
            <Number
              key={index}
              {...box}
              locked={box.locked || locked || isGameOver}
              index={index}
              handler={() => handler(index)}
            />
          );
        })}
        <Bonus checked={locked} handler={lockHandler} />
      </div>
    </div>
  );
};

export default Row;
