import React from "react";

interface IBonusProps {
  checked: boolean;
  handler: Function;
}

const Bonus: React.FC<IBonusProps> = ({ checked, handler }) => {
  return (
    <div className="Bonus NumberBox" onClick={() => handler()}>
      <div className="content">{checked ? "X" : ""}</div>
    </div>
  );
};

export default Bonus;
