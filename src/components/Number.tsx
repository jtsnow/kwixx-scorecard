import React from "react";

import { boxClickHandler, IBoxData } from "../types";

interface IBoxElementProps {
  index: number;
  handler: boxClickHandler;
}

type BoxProps = IBoxData & IBoxElementProps;

const Number: React.FC<BoxProps> = ({ checked, locked, value, handler }) => {
  const checkedClass = checked ? "checked" : "unchecked";
  const lockedClass = locked ? "locked" : "";
  return (
    <div
      className={`NumberBox ${checkedClass} ${lockedClass}`}
      onClick={() => handler()}
    >
      <div className="content">{value}</div>
    </div>
  );
};

export default Number;
