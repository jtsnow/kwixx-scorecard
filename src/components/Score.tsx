import React from "react";

interface IScoreProps {
  rowScores: number[];
  penalties: boolean[];
}

const Score: React.FC<IScoreProps> = ({ rowScores, penalties }) => {
  const penaltyScore = penalties.filter((x) => x).length * 5;
  const totalScore = rowScores.reduce((tot, n) => tot + n) - penaltyScore;

  return (
    <div className="ScoreContainer">
      <div key="red" className={`ScoreBox rowScore red`}>
        {rowScores[0]}
      </div>
      <div className="symbol">+</div>
      <div key="yellow" className={`ScoreBox rowScore yellow`}>
        {rowScores[1]}
      </div>
      <div className="symbol">+</div>
      <div key="green" className={`ScoreBox rowScore green`}>
        {rowScores[2]}
      </div>
      <div className="symbol">+</div>
      <div key="blue" className={`ScoreBox rowScore blue`}>
        {rowScores[3]}
      </div>
      <div className="symbol">-</div>
      <div className="ScoreBox penaltyScore">{penaltyScore}</div>
      <div className="symbol">=</div>
      <div className="ScoreBox totalScore">{totalScore}</div>
    </div>
  );
};

export default Score;
