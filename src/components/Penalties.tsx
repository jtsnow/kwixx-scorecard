import React from "react";

interface IPenaltyProps {
  state: boolean[];
  handler: Function;
}

function getClass(checked: boolean) {
  return checked ? "checked" : "";
}

const Penalties: React.FC<IPenaltyProps> = ({ state, handler }) => {
  return (
    <div className="Penalties">
      {state.map((checked, i) => {
        return (
          <div
            key={i}
            className={`PenaltyBox ${getClass(checked)}`}
            onClick={() => handler(i)}
          >
            {checked ? "X" : ""}
          </div>
        );
      })}
    </div>
  );
};

export default Penalties;
