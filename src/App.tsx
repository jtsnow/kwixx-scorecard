import React, { useState } from "react";
import "./App.css";

import { clickHandler, IScore, IRowData } from "./types";
import {
  computeRowScore,
  countLockedRows,
  countPenalties,
  getLastChecked,
  getNumChecked,
} from "./util";
import { getInitRows, getInitScore } from "./util/init";
import ConfirmationDialog from "./components/ConfirmationDialog";
import Penalties from "./components/Penalties";
import ResetButton from "./components/ResetButton";
import Row from "./components/Row";
import Score from "./components/Score";
import usePersistedState from "./util/state";

function App() {
  const [rows, setRows] = usePersistedState<IRowData[]>(getInitRows(), "rows");
  const [score, setScore] = usePersistedState<IScore>(getInitScore(), "score");
  const [isDialogOpen, setDialogOpen] = useState<boolean>(false);

  const lockedRowCount = countLockedRows(rows);
  const penaltyCount = countPenalties(score.penalties);
  const isGameOver = lockedRowCount === 2 || penaltyCount === 4;

  const resetGame = () => {
    setRows(getInitRows());
    setScore(getInitScore());
    setDialogOpen(false);
  };

  const handleClick: clickHandler = (rowIndex: number) => {
    return (boxIndex: number) => {
      const newRows = rows.slice();
      const row = newRows[rowIndex];
      const clickedBox = row.boxes[boxIndex];
      const isLastBox = boxIndex === row.boxes.length - 1;
      if (isLastBox && clickedBox.checked) {
        row.locked = false;
        clickedBox.locked = false;
      } else if (row.locked || clickedBox.locked || isGameOver) {
        return;
      }

      clickedBox.checked = !clickedBox.checked;

      const lastChecked = getLastChecked(row.boxes, boxIndex);

      row.boxes = row.boxes.map((box, index) => {
        box.locked = index < lastChecked ? true : false;
        return box;
      });

      const numChecked = getNumChecked(row.boxes);
      const lastBox = row.boxes[row.boxes.length - 1];
      lastBox.locked = numChecked < 5;
      row.locked = lastBox.checked;

      const newScore: IScore = {
        rowScores: newRows.map((row) => computeRowScore(row.boxes)),
        penalties: score.penalties,
      };
      setRows(newRows);
      setScore(newScore);
    };
  };

  const handlePenaltyClick = (index: number) => {
    const penalties = score.penalties.slice();
    if (isGameOver && penaltyCount !== 4) {
      return;
    }
    penalties[index] = !penalties[index];
    setScore({ rowScores: score.rowScores.slice(), penalties });
  };

  const handleLockClick = (rowIndex: number) => {
    return () => {
      const newRows = rows.slice();
      const row = newRows[rowIndex];
      const lastBox = row.boxes[row.boxes.length - 1];
      if (lastBox.checked || (isGameOver && !row.locked)) {
        return;
      }

      row.locked = !row.locked;
      setRows(newRows);
    };
  };

  return (
    <div className="App">
      <section className="grid">
        <div className="RowContainer">
          {rows.map((row, index) => {
            return (
              <Row
                key={row.color}
                {...row}
                index={index}
                handler={handleClick(index)}
                lockHandler={handleLockClick(index)}
                isGameOver={isGameOver}
              />
            );
          })}
        </div>
      </section>
      <section className="penalties">
        <ResetButton onClick={() => setDialogOpen(true)}></ResetButton>
        <Penalties state={score.penalties} handler={handlePenaltyClick} />
      </section>
      <section className="score">
        <Score {...score} />
      </section>
      <ConfirmationDialog
        open={isDialogOpen}
        onConfirm={resetGame}
        onCancel={() => setDialogOpen(false)}
      ></ConfirmationDialog>
    </div>
  );
}

export default App;
