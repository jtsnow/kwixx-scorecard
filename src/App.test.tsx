import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders learn react link", () => {
  const { getAllByText } = render(<App />);
  const twelves = getAllByText(/12/i);
  expect(twelves.length).toEqual(4);
});
