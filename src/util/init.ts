import { range } from "./";
import { IScore, IRowData } from "../types";

export function getInitScore(): IScore {
  return {
    rowScores: [0, 0, 0, 0],
    penalties: [false, false, false, false],
  };
}

export function getInitRows(): IRowData[] {
  const initRows: IRowData[] = [];
  initRows.push({
    boxes: range(11, 2),
    locked: false,
    color: "red",
  });
  initRows.push({
    boxes: range(11, 2),
    locked: false,
    color: "yellow",
  });
  initRows.push({
    boxes: range(11, 2).reverse(),
    locked: false,
    color: "green",
  });
  initRows.push({
    boxes: range(11, 2).reverse(),
    locked: false,
    color: "blue",
  });
  initRows[0].boxes[10].locked = true;
  initRows[1].boxes[10].locked = true;
  initRows[2].boxes[10].locked = true;
  initRows[3].boxes[10].locked = true;
  return initRows;
}
