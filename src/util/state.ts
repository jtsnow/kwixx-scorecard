import React, { useState } from "react";

function usePersistedState<T>(
  initialState: T | (() => T),
  key: string
): [T, React.Dispatch<React.SetStateAction<T>>] {
  const fromStorage = JSON.parse(localStorage.getItem(key) || "false");
  const [state, setState] = useState<T>(fromStorage || initialState);

  const setPersistedState = (value: React.SetStateAction<T>) => {
    localStorage.setItem(key, JSON.stringify(value));
    return setState(value);
  };
  return [state, setPersistedState];
}

export default usePersistedState;
