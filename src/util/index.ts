import { IBoxData, IRowData } from "../types";

export function range(size: number, startAt: number = 0): IBoxData[] {
  return Array.from(Array(size).keys()).map((i) => {
    return { value: (i + startAt).toString(), locked: false, checked: false };
  });
}

export function getLastChecked(boxes: IBoxData[], start?: number): number {
  const startIndex =
    start === undefined || start > boxes.length - 1 ? boxes.length - 1 : start;

  for (let i = startIndex; i >= 0; i--) {
    if (boxes[i].checked) {
      return i;
    }
  }
  return -1;
}

export function lockLastBox(boxes: IBoxData[]): IBoxData[] {
  const newBoxes = boxes.slice();
  newBoxes[newBoxes.length - 1].locked = true;
  return newBoxes;
}

export function unlockLastBox(boxes: IBoxData[]): IBoxData[] {
  const newBoxes = boxes.slice();
  newBoxes[newBoxes.length - 1].locked = false;
  return newBoxes;
}

const scoreMap = new Map<number, number>();
scoreMap.set(1, 1);
scoreMap.set(2, 3);
scoreMap.set(3, 6);
scoreMap.set(4, 10);
scoreMap.set(5, 15);
scoreMap.set(6, 21);
scoreMap.set(7, 28);
scoreMap.set(8, 36);
scoreMap.set(9, 45);
scoreMap.set(10, 55);
scoreMap.set(11, 66);
scoreMap.set(12, 78);

export function computeRowScore(boxes: IBoxData[]): number {
  const numChecked = getNumChecked(boxes);
  const bonus = numChecked > 5 && boxes.slice(-1).pop()?.checked ? 1 : 0;
  const totalChecked = numChecked + bonus;
  return scoreMap.get(totalChecked) || 0;
}

export function getNumChecked(boxes: IBoxData[]): number {
  return boxes.filter((box) => box.checked).length;
}

export function countLockedRows(rows: IRowData[]): number {
  return rows.filter((row) => row.locked).length;
}

export function countPenalties(penalties: boolean[]): number {
  return penalties.filter((val) => val).length;
}
