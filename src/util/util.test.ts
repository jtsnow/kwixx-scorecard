import { computeRowScore, getLastChecked, range } from "./index";
import { IBoxData } from "../types";

// test('renders learn react link', () => {
//   const { getByText } = render(<App />);
//   const linkElement = getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

function fillBoxes(n: number): IBoxData[] {
  return buildBoxes(Array<boolean>(11).fill(false).fill(true, 0, n));
}

function buildBoxes(boxes: boolean[]): IBoxData[] {
  return boxes.map((val, i) => {
    return { checked: val, value: (i + 2).toString(), locked: false };
  });
}

function checkLastBox(boxes: IBoxData[]): IBoxData[] {
  const newBoxes = boxes.slice();
  newBoxes[newBoxes.length - 1].checked = true;
  return newBoxes;
}

test("gets index of last checked number", () => {
  expect(getLastChecked(buildBoxes([false, false, true]), 2)).toEqual(2);
  expect(getLastChecked(buildBoxes([false, false, false]), 2)).toEqual(-1);
  expect(getLastChecked(buildBoxes([true, false, false]), 0)).toEqual(0);
  expect(getLastChecked(buildBoxes([true, false, false]), 2)).toEqual(0);
});

test("computes row score", () => {
  expect(computeRowScore(fillBoxes(1))).toEqual(1);
  expect(computeRowScore(fillBoxes(2))).toEqual(3);
  expect(computeRowScore(fillBoxes(3))).toEqual(6);
  expect(computeRowScore(fillBoxes(4))).toEqual(10);
  expect(computeRowScore(fillBoxes(5))).toEqual(15);
  expect(computeRowScore(fillBoxes(6))).toEqual(21);
  expect(computeRowScore(fillBoxes(7))).toEqual(28);
  expect(computeRowScore(fillBoxes(8))).toEqual(36);
  expect(computeRowScore(fillBoxes(9))).toEqual(45);
  expect(computeRowScore(fillBoxes(10))).toEqual(55);
  expect(computeRowScore(fillBoxes(11))).toEqual(78);
});

test("computes row score with bonus", () => {
  expect(computeRowScore(checkLastBox(fillBoxes(5)))).toEqual(28);
  expect(computeRowScore(checkLastBox(fillBoxes(6)))).toEqual(36);
  expect(computeRowScore(checkLastBox(fillBoxes(7)))).toEqual(45);
  expect(computeRowScore(checkLastBox(fillBoxes(8)))).toEqual(55);
  expect(computeRowScore(checkLastBox(fillBoxes(9)))).toEqual(66);
  expect(computeRowScore(checkLastBox(fillBoxes(10)))).toEqual(78);
});

test("computes row score without bonus", () => {
  expect(computeRowScore(checkLastBox(fillBoxes(0)))).toEqual(1);
  expect(computeRowScore(checkLastBox(fillBoxes(1)))).toEqual(3);
  expect(computeRowScore(checkLastBox(fillBoxes(2)))).toEqual(6);
  expect(computeRowScore(checkLastBox(fillBoxes(3)))).toEqual(10);
  expect(computeRowScore(checkLastBox(fillBoxes(4)))).toEqual(15);
});
