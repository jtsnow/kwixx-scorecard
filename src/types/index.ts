export interface IRowData {
  boxes: IBoxData[];
  locked: boolean;
  color: string;
}

export interface IBoxData {
  value: string;
  checked: boolean;
  locked: boolean;
}

export interface IScore {
  rowScores: number[];
  penalties: boolean[];
}

export type clickHandler = (i: number) => (j: number) => void;
export type rowClickHandler = (j: number) => void;
export type boxClickHandler = () => void;
